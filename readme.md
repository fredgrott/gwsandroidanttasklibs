GWSAndroidANTTaskLibs
---

# Purpose

Apache IVY has this nice feature to allow you set-up local and non-local 
repos thus we are using that to set up a local repo for android projects.  
Can be used either with Ant, Maven, Gradle, or Buildr.

# Artifact Naming

I kept the original artifacts names so its in the pattern of:

artifact.ext

Not all these are in any maven or ivy repo and thus I know that this group will 
always be loaded from a local repo.

I also use a flatfile style originization of the setup repo that the local 
repos icreated from.

# Configurations Used

The Ivy confgiurations I use are:

app-project
instrumented-test
lib-project
javamonkey

you can fork and change those configurations if you want.

# Libraries Included

The following libraries are included:

classycle

checkstyle

zutubi android version ant task

javancss

jdepend

pmd

# Usage

Download and execute the ant task and that will create a local repo in your user home directory.
The only difference in build script approaches is in Apache ANT your use the Apache IVY jar as
your build boot up jar to load in rest of the ant lib tasks that are not in the android sdk while in
something liek Gralde IVY support is included thus no need to bootstrap with an ivy jar.

# License

Licensed under Apache 2.0 licence:

[Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0.html)

# Project Feedback

You can give project feedback by emailing Fred Grott, the project lead:

[Fred Grott's Dev Blog, email link right in front page](http://share.me.github.com)

# Credits

Fred Grott's own creation.
